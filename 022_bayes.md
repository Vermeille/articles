As some smart people claim that the human brain is a bayesian machine, it's
worth investigating its flaws to understand the spread of beliefs and false
ideas. This posts studies *psychology* through maths. But it's about psychology
first.

Let's introduce Bayes' Theorem and study its flaws. Bayes is also absolutely
wonderful, but [this
article](http://blogs.scientificamerican.com/cross-check/bayes-s-theorem-what-s-the-big-deal/)
from Scientific American will explain this better than I could. It's a good
read.

## Bayes' Theorem

Bayes' Theorem may be stated with a simple and intuitive sentence : when your
beliefs are faced with evidence, you update those beliefs according to this
evidence. Another easier way to state it, is that you just adjust your
hypothesis on what you live and experience. That's actually sound, and makes a
lot of sense. What's wrong with it? Well, how can people living in the same
country (and then experiencing almost the same evidence) have very different
political opinions? Or see God's hands in their everyday lives while others
only see contingency?

Let's first describe this theorem:

$$P(H|E) = \frac{P(H)P(E|H)}{P(E}$$

$E$ stands for Evidence (observation), and $H$ for Hypothesis (belief). This,
in plain english, says "What I will believe after an event ($P(H|E)$) is defined
by how much I believed this thing before ($P(H)$) and how much this belief
explains what I'm seeing ($P(E|H)$)". We disregard the denominator as it does
not involves the hypothesis and then stays the same when we evaluate several
hypothesis, it behaves as a normalisation parameter, which means that it makes
sure that the sum of belief is always 1.

Again, this is perfectly fine, and often used in science. Read the article from
SA.

However, everybody will warn you about $P(H)$, which is technically called "a
prior", the beliefs you hold before the observation. Bayesianists will tell you
to use smart priors: either one obtained with careful science, measures, and
statistics, or, if you can't, use an uninformative prior, which means "one that
does not favor any hypothesis".

## Be careful with the prior

However, very few humans have "uninformative priors". We are educated in a
(non-)religious family, with certain political ideas, we tend to favor
hypothesis we like, etc. So, human priors are hardly uninformative, and tend to
be heavily biased and not supported by evidence or rational thinking.

The flaw is simple : what happens when a biased prior meets an uninformative
evidence?

Let $H$ be my belief that "when my imaginary friend wants, my uncle gives me
money", that I believe about 75%, ie 0.75, because my mom told me so when I was
a child; and the null hypothesis "my imaginary friend has no influence on my uncle",
with a prior belief of 0.25. Thankfully, last friday, my uncle gave me some
money. What happens?

With $H$ = "imaginary friend does have an influence":
* $P(H) = 0.75$, because that's my prior belief
* $P(E|H) = 0.1$, because my uncles gives me money approximately three times a
  month.

After this event, $$P(H|E) = \frac{0.75 \times 0.1 = 0.075}{P(E)}$$

Now, I consider my belief about the null hypothesis:
* $P(\neg H) = 0.25$, because I don't really believe so
* $P(E|\neg H) = 0.1$, because my uncles *still* gives me money approximately three
  times a month, no matter what I think.

After this event, $$P(\neg H|E) = \frac{0.25 \times 0.1 = 0.025}{P(E)}$$

If we want to compare them, $P(E)$ cancels out, and I *still* belive that my
imaginary friend has some influence because my belief has no observable
causality on my observation. In the frequentist framework, this issue would not
arise.

If we normalise this to have a new belief distribution (summing to 1), we would
see that the beliefs remains unchanged. But we can do even worse. Let say that
my imaginary friend is in a better mood on Monday, and fortunately, Monday is
my uncle's payday, so he's more likely to give me money on that day, $P(E|H)$
increases... and so does $P(H|E).

