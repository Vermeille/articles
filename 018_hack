No, I can't hack your ex-girlfriend's Facebook
----------------------------------------------

As a computer geek who eventually turned into a software engineer, one of the questions I've been
asked the most (if not *the most*), is

> Can you hack someone's service account?

And I'm sure it's super common among SWEs, and I'm sure I'm not the only one raging about that.

## Quick and simple answer

No, I fuckin' can't.

## Longer and detailed answer

To understand why, let me explain what it takes to hack something.

### An internet service

Any internet service (GMail, Facebook, MSN, any blogging service, etc) is a (complex) system built
by engineers. Compare it to an engine, in a mechanical sense, or an electronic circuit, if you
like, in a black box. This service is cut into many different parts:

* The web view is what you actually see and quite often the only way you have to use the website.
  On a minimal or poorly designed website, you directly receive the view filled with the data (the
  content of the page).

* Sometimes, this webview calls another service on the web. For exemple, when you go on Facebook,
  you want to see the webview. When it's showing, the webview queries another service on the web to
  know what it must show, this service is known as an API. The API sends data back to the webview
  in an easy to process but very not pretty way.

* Both of them are generated on a webserver where the company's code is running. You have
  absolutely no control on that. Actually, that's all of what hacking is about: be able to access
  this server and do bad stuff.

* Most of the time, there's also a database service where all of user data is stored.
  This is the most precious piece of such services. Breaking into this is actually even better than
  the webserver. Engineers know that. That's why it has most often no interaction to the external
  world. It's directly accessible only from the inside of the company, and very few people have the
  credentials. For such big companies, most engineers never see actual data, and work only on
  generated and fictious data for privacy concerns, some being by company's ethics, other enforced
  by law. So that data can be actually rendered to the user, the webserver queries the database
  about what's needed, filters the result and renders it to the webview or the API.

### How do we get in?

You have to find a weakness of the system. You have to *misuse* it. If we continue with the
analogies proposed before, the question of hacking is:

> Given what we could see and how we could interact with the engine, how can we misuse it so that
> it does what we want?

If you have a car with an artificial speed limitation, you have to understand how the engine works,
why it has this limitation, and how you can remove it. Not that hard'uh? Everybody at least a
little known in mechanics can do that! Sure thing SWEs know how software works and can do the same!

Nope. In the case of an internet service, you can't even touch the engine. You can only see what it
produces, and use it in the way engineers allowed you. You don't know how it works because most of
the time the company keeps it secret. That's trying to bypass the car's speed limitation by only
using the car's controls. Now it sounds way harder, doesn't it?

### What is hacking about, in the end?

Hacking is misusing you service, trying to guess what are the mistakes the engineers could have
done, so that you can do more than what you should. It's about forgetting to handle server's state
properly [when doing two opposite actions](http://sakurity.com/blog/2015/05/21/starbucks.html);
not securing some text fields, allowing you to
[inject code](http://www.w3schools.com/sql/sql_injection.asp) (super old, everybody's aware about
this one), not checking properly filetypes when uploading files etc.

So, the hacker has to guess, try, fail, imagine what the engineers have done wrong. Try everything
that comes to his mind, and, hopefully, that'll work. And even if you find a weakness, you have no
clue that you can exploit it to do what you'd like. Maybe it will just produce garbage results.

### I know a weakness and how to exploit it

Good, you have two choices now:

1. Contact the service, and tell them about it. They'll thank you, you'll save people data, maybe
can write an article that will get famous for few weeks, and be given some money by the said
company.

2. Keep it for yourself. Obviously, if it gets known, they will eventually fix it, and it will
become unusable.

So, exploits are well kept secrets. No, I can't hack your ex-girlfriend's account.

## But I had my account hacked!

Well, odds are it's not Facebook's fault. As long as a service has users, there is a major
vulnerability: the user's stupidity. Users are dumb. Beyond imagination. They write their passwords
in a non secured file on their computers, they use public data like their birthday, or they use
publicly available answer to their secret question (seriously guys, just enter garbage if you're
asked. And if you want to "hack" someone you know, just ask them for the answer, they'll never
remind this question can leak their accounts), they use the same password on every service, they
leave their computers unlocked, they believe phishing, etc etc.

If you've been hacked, chances are that you've been stupid at some point. If you want to hack
someone, just exploit his stupidity and send a phishing page.

## Let's say I'm stupid, what could I do about that?

Make sure your computer is clean: do not install garbage (like toolbars) or from untrusted sources.
Use a password manager like LastPass to have random passwords to everything and keep your passwords
safe. Read the links you're clicking on, and check the URL when you're asked your credentials.

